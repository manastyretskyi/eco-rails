Rails.application.routes.draw do
  get 'sessions/new'
  get 'sessions/create'
  get 'sessions/destroy'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  root 'welcome#index', as: 'start'
  resources :users, only: [:new, :create]
  get '/sign_up', to: 'users#new', as: :sign_up

  resources :sessions, only: [:new, :create, :destroy]
  get '/log_in', to: 'sessions#new', as: :log_in
  get '/log_out', to: 'sessions#destroy', as: :log_out

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
