class User
  include Mongoid::Document
  authenticates_with_sorcery!
  validates :email, uniqueness: true

end
